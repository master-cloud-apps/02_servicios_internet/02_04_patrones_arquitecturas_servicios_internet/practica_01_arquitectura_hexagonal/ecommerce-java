package org.eyo.architecture.hexagonal.ecommerce.shoppingcart.domain.dto;

public class CreateShoppingCartDTO {

    public CreateShoppingCartDTO() {
    }

    public CreateShoppingCartDTO(String status) {
        this.status = status;
    }

    private String status;

    public String getStatus() {
        return status;
    }
}
