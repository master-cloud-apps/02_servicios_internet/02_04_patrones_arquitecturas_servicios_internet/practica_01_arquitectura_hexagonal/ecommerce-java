package org.eyo.architecture.hexagonal.ecommerce.product.domain.dto;

public class CreateProductRequestDTO {
    private String kind;
    private String name;
    private String description;

    public CreateProductRequestDTO() {
    }

    public CreateProductRequestDTO(String kind, String name, String description) {
        this.kind = kind;
        this.name = name;
        this.description = description;
    }

    public String getKind() {
        return kind;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }
}
