package org.eyo.architecture.hexagonal.ecommerce.shoppingcart.application.service;

import org.eyo.architecture.hexagonal.ecommerce.shoppingcart.domain.dto.CreateShoppingCartDTO;
import org.eyo.architecture.hexagonal.ecommerce.shoppingcart.domain.dto.ShoppingCartDTO;
import org.eyo.architecture.hexagonal.ecommerce.shoppingcart.domain.port.ShoppingCartUseCase;
import org.springframework.stereotype.Service;

@Service
public class ShoppingCartServiceImpl implements ShoppingCartService{
    private ShoppingCartUseCase shoppingCartUseCase;

    public ShoppingCartServiceImpl(ShoppingCartUseCase shoppingCartUseCase) {
        this.shoppingCartUseCase = shoppingCartUseCase;
    }

    @Override
    public ShoppingCartDTO createShoppingCart(CreateShoppingCartDTO shoppingCartInput) {
        return this.shoppingCartUseCase.createShoppingCart(shoppingCartInput);
    }

    @Override
    public ShoppingCartDTO endCart(Long cartId) {
        return this.shoppingCartUseCase.endCart(cartId);
    }

    @Override
    public ShoppingCartDTO getShoppingCart(Long cartId) {
        return this.shoppingCartUseCase.getShoppingCart(cartId);
    }

    @Override
    public ShoppingCartDTO addProductToShoppingCart(Long cartId, Long productId, Integer quantity) {
        return this.shoppingCartUseCase.addProductToShoppingCart(cartId, productId, quantity);
    }

    @Override
    public ShoppingCartDTO deleteProductFromCart(Long cartId, Long productId) {
        return this.shoppingCartUseCase.deleteProductFromCart(cartId, productId);
    }

    @Override
    public ShoppingCartDTO deleteCart(Long cartId) {
        return this.shoppingCartUseCase.deleteCart(cartId);
    }
}
