package org.eyo.architecture.hexagonal.ecommerce.product.domain.port;

import org.eyo.architecture.hexagonal.ecommerce.product.domain.dto.CreateProductRequestDTO;
import org.eyo.architecture.hexagonal.ecommerce.product.domain.dto.ProductDTO;

import java.util.List;

public interface ProductUseCase {
    ProductDTO create(CreateProductRequestDTO productToCreate);

    ProductDTO deleteProduct(Long productId);

    ProductDTO getProduct(Long productId);

    ProductDTO updateProduct(ProductDTO productToUpdate);

    List<ProductDTO> getProducts();
}
