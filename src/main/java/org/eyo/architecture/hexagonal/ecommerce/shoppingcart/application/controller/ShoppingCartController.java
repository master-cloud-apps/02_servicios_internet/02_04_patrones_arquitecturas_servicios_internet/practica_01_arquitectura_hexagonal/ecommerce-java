package org.eyo.architecture.hexagonal.ecommerce.shoppingcart.application.controller;

import org.eyo.architecture.hexagonal.ecommerce.shoppingcart.application.service.ShoppingCartService;
import org.eyo.architecture.hexagonal.ecommerce.shoppingcart.domain.dto.CreateShoppingCartDTO;
import org.eyo.architecture.hexagonal.ecommerce.shoppingcart.domain.dto.ShoppingCartDTO;
import org.eyo.architecture.hexagonal.ecommerce.shoppingcart.domain.port.ShoppingCartUseCase;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;

import static org.eyo.architecture.hexagonal.ecommerce.utils.AppConstants.SHOPPING_CART_URL;
import static org.springframework.web.servlet.support.ServletUriComponentsBuilder.fromCurrentRequest;

@RestController
@RequestMapping(SHOPPING_CART_URL)
public class ShoppingCartController {

    private ShoppingCartService shoppingCartService;

    public ShoppingCartController(ShoppingCartService shoppingCartService) {
        this.shoppingCartService = shoppingCartService;
    }

    @PostMapping("")
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<Void> createShoppingCart(@RequestBody CreateShoppingCartDTO shoppingCartDTO) {
        ShoppingCartDTO createdCart = this.shoppingCartService.createShoppingCart(shoppingCartDTO);
        URI location = fromCurrentRequest().path("/{id}").buildAndExpand(createdCart.getId()).toUri();
        return ResponseEntity.created(location).build();
    }

    @GetMapping("/{cartId}")
    public ResponseEntity<ShoppingCartDTO> getShoppingCartById(@PathVariable Long cartId) {
        if (this.shoppingCartService.getShoppingCart(cartId) == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(this.shoppingCartService.getShoppingCart(cartId));
    }

    @PatchMapping("/{cartId}")
    public ResponseEntity<ShoppingCartDTO> updateShoppingCart(@PathVariable Long cartId) {
        if (this.shoppingCartService.getShoppingCart(cartId) == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(this.shoppingCartService.endCart(cartId));
    }

    @DeleteMapping("/{cartId}")
    public ResponseEntity<Void> deleteShoppingCartById(@PathVariable Long cartId) {
        ShoppingCartDTO cartToDelete = this.shoppingCartService.getShoppingCart(cartId);

        if (cartToDelete == null) {
            return ResponseEntity.notFound().build();
        }
        this.shoppingCartService.deleteCart(cartId);
        return ResponseEntity.noContent().build();
    }

    @PostMapping("/{cartId}/product/{productId}/quantity/{prodQuantity}")
    public ResponseEntity<ShoppingCartDTO> createAddProductToShoppingCart(@PathVariable Long cartId,
                                                                          @PathVariable Long productId,
                                                                          @PathVariable Integer prodQuantity) {
        ShoppingCartDTO cartWithProdAdded = this.shoppingCartService.addProductToShoppingCart(cartId, productId,
                prodQuantity);
        if (cartWithProdAdded == null){
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(cartWithProdAdded);
    }

    @DeleteMapping("/{cartId}/product/{productId}")
    public ResponseEntity<ShoppingCartDTO> removeProductFromCart(@PathVariable Long cartId,
                                                                          @PathVariable Long productId) {
        ShoppingCartDTO cartWithProdAdded = this.shoppingCartService.deleteProductFromCart(cartId, productId);
        if (cartWithProdAdded == null){
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(cartWithProdAdded);
    }
}
