package org.eyo.architecture.hexagonal.ecommerce.shoppingcart.domain;

import org.eyo.architecture.hexagonal.ecommerce.product.domain.Product;
import org.eyo.architecture.hexagonal.ecommerce.shoppingcart.domain.dto.CartItemDTO;

public class CartItem {

    private Product product;
    private Integer quantity;

    public CartItem(CartItemDTO cartItemDTO) {
        this.product = new Product(cartItemDTO.getProduct());
        this.quantity = cartItemDTO.getQuantity();
    }

    public CartItem(Product product, Integer quantity) {
        this.product = product;
        this.quantity = quantity;
    }

    public Long getProductId() {
        return this.product.getId();
    }

    public Product getProduct() {
        return this.product;
    }

    public Integer getQuantity() {
        return this.quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }
}
