package org.eyo.architecture.hexagonal.ecommerce.product.infraestructure.repository;

import org.eyo.architecture.hexagonal.ecommerce.product.domain.port.ProductRepository;
import org.eyo.architecture.hexagonal.ecommerce.product.domain.dto.ProductDTO;
import org.modelmapper.ModelMapper;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

public class ProductMockRepository implements ProductRepository {
    private ModelMapper modelMapper = new ModelMapper();
    private Map<Long, ProductDTO> productDB = new HashMap<>();

    @Override
    public ProductDTO save(ProductDTO productDTO) {
        ProductDTO savedProduct = modelMapper.map(productDTO, ProductDTO.class);
        if (productDTO.getId() == null)
            savedProduct.setId((long) this.productDB.values().size());
        this.productDB.put(savedProduct.getId(), savedProduct);
        return savedProduct;
    }

    @Override
    public ProductDTO findById(Long productId) {
        return this.productDB.get(productId);
    }

    @Override
    public ProductDTO deleteById(Long productId) {
        return this.productDB.remove(productId);
    }

    @Override
    public List<ProductDTO> getProducts() {
        return this.productDB.values().stream().collect(Collectors.toList());
    }
}
