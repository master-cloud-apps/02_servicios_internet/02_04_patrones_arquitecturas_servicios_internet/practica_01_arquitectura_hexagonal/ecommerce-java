package org.eyo.architecture.hexagonal.ecommerce.shoppingcart.infraestructure.repository;

import org.eyo.architecture.hexagonal.ecommerce.product.domain.dto.ProductDTO;
import org.eyo.architecture.hexagonal.ecommerce.shoppingcart.domain.dto.ShoppingCartDTO;
import org.eyo.architecture.hexagonal.ecommerce.shoppingcart.domain.port.ShoppingCartRepository;
import org.modelmapper.ModelMapper;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

public class ShoppingCartMockRepository implements ShoppingCartRepository {
    private ModelMapper modelMapper = new ModelMapper();
    private Map<Long, ShoppingCartDTO> shoppingCartDB = new HashMap<>();


    @Override
    public ShoppingCartDTO save(ShoppingCartDTO shoppingCartDTO) {
        ShoppingCartDTO savedShoppingCart = modelMapper.map(shoppingCartDTO, ShoppingCartDTO.class);
        if (savedShoppingCart.getId() == null)
            savedShoppingCart.setId((long) this.shoppingCartDB.values().size());
        this.shoppingCartDB.put(savedShoppingCart.getId(), savedShoppingCart);
        return savedShoppingCart;
    }

    @Override
    public ShoppingCartDTO findById(Long shoppingCartId) {
        return this.shoppingCartDB.get(shoppingCartId);
    }

    @Override
    public ShoppingCartDTO deleteById(Long shoppingCartId) {
        return this.shoppingCartDB.remove(shoppingCartId);
    }

}
