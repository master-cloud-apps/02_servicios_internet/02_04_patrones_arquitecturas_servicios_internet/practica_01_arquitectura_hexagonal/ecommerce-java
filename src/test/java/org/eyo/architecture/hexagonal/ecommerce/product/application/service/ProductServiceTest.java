package org.eyo.architecture.hexagonal.ecommerce.product.application.service;

import org.eyo.architecture.hexagonal.ecommerce.product.domain.dto.CreateProductRequestDTO;
import org.eyo.architecture.hexagonal.ecommerce.product.domain.dto.ProductDTO;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.eyo.architecture.hexagonal.ecommerce.TestConstants.*;
import static org.junit.jupiter.api.Assertions.*;

@Disabled
@SpringBootTest
class ProductServiceTest {

    public static final String NEW_DESCRIPTION = "New description";
    @Autowired
    private ProductService productService;
    @Autowired
    private ModelMapper modelMapper;

    @Test
    void shouldValidateCRUD() {
        ProductDTO productCreated = this.productService.create(new CreateProductRequestDTO(BOOK_KIND,
                BOOK_NAME, BOOK_DESCRIPTION));
        assertNotNull(this.productService.getProduct(productCreated.getId()));
        assertEquals(BOOK_NAME, this.productService.getProduct(productCreated.getId()).getName());

        ProductDTO productToUpdate = this.modelMapper.map(productCreated, ProductDTO.class);
        productToUpdate.setDescription(NEW_DESCRIPTION);
        ProductDTO productUpdated = this.productService.updateProduct(productToUpdate);
        assertEquals(NEW_DESCRIPTION, productUpdated.getDescription());
        assertEquals(NEW_DESCRIPTION, this.productService.getProduct(productToUpdate.getId()).getDescription());

        this.productService.deleteProduct(productUpdated.getId());
        assertNull(this.productService.getProduct(productCreated.getId()));
    }

}
